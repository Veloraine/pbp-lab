from django.db import models
from django.forms import ModelForm, DateInput, widgets
from lab_1.models import Friend

class DateInput(DateInput):
    input_type = "date"
class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields = "__all__"
        widgets = {'dob' : DateInput()}
